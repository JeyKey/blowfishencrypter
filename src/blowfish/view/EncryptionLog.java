package blowfish.view;

import javafx.scene.control.TextArea;

/**
 * Created by Jaro on 30/03/16.
 */
public class EncryptionLog extends TextArea {

    boolean loading;
    Thread loadingThread;

    public EncryptionLog() {
        super();
        loading = true;
    }

    public void print(String s) {
        appendText(s);

    }

    public void println(String s) {
        appendText(s + "\n");

    }

    public void startLoading() {

        loading = true;
        loadingThread = new Thread(() -> {
            try {
                while (loading) {
                    appendText(". ");
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        loadingThread.start();
    }

    public void stopLoading() {
        loading = false;
        while(loadingThread.isAlive());
    }
}
