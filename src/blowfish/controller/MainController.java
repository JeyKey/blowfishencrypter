package blowfish.controller;

import blowfish.model.encryption.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import blowfish.model.*;
import blowfish.view.EncryptionLog;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.security.auth.DestroyFailedException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private EncryptionFile inputFile;
    private EncryptionFile outputFile;

    private void setActiveMode(Mode activeMode) {
        this.activeMode = activeMode;
    }

    void setStage(Stage stage) {
        this.stage = stage;
    }


    private enum Mode {
        encrypt,
        decrypt
    }

    @FXML
    TextArea inputFilePath;
    @FXML
    TextArea outputFilePath;

    @FXML
    ChoiceBox<EncryptionMode> encryptionModeChoiceBox;
    @FXML
    TextField keyLengthTextField;
    @FXML
    Slider keyLengthSlider;

    @FXML
    GridPane subBlockGridPane;
    @FXML
    TextField subBlockLengthTextField;
    @FXML
    Slider subBlockLengthSlider;

    @FXML
    PasswordField privateKeyPasswordField;

    @FXML
    ListView<User> userListView;

    @FXML
    TabPane tabPane;
    @FXML
    TextField status;
    @FXML
    EncryptionLog log;

    private Stage stage;
    private ObservableList<User> userList;
    private ArrayList<User> recipients;
    private Mode activeMode;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        skipJCEPolicy();
        initFiles();
        initActiveMode();
        initRecipients();
        initEncryptionModeChoiceBox();
        initKeySlider();
        initKeyLengthTextField();
        initSubBlockSlider();
        initTabPane();
        initUserList();
    }

    private void skipJCEPolicy() {

        try {
            ignoreJCERestriction();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void ignoreJCERestriction() throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
        field.setAccessible(true);
        field.set(null, Boolean.FALSE);
    }

    private void initFiles() {
        inputFile = null;
        outputFile = null;
        inputFilePath.setText(null);
        outputFilePath.setText(null);
        inputFilePath.setPromptText("Please select input file");
        outputFilePath.setPromptText("Please select output file");
    }

    private void initKeyLengthTextField() {
        keyLengthTextField.setText(String.valueOf(((int) keyLengthSlider.getValue())));
    }

    private void initActiveMode() {
        setActiveMode(Mode.encrypt);
    }

    private void initKeySlider() {
        keyLengthSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            keyLengthChanged(newValue.intValue());
        });
    }

    private void initRecipients() {
        recipients = new ArrayList<>();
    }

    private void initTabPane() {
        tabPane.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            modeChanged(newValue.getText());
        }));
    }

    private void initSubBlockSlider() {
        subBlockLengthSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            subBlockLengthChanged(newValue.intValue());
        });
        subBlockLengthTextField.setText(String.valueOf((int) subBlockLengthSlider.getValue()));
    }

    private void initEncryptionModeChoiceBox() {
        encryptionModeChoiceBox.setItems(FXCollections.observableArrayList(EncryptionMode.values()));
        encryptionModeChoiceBox.getSelectionModel().selectedIndexProperty().addListener(((observable, oldValue, newValue) -> {
            encryptionModeChanged(EncryptionMode.values()[newValue.intValue()]);
        }));
        encryptionModeChanged(EncryptionMode.ECB);
    }

    private void initUserList() {

        userList = FXCollections.observableArrayList();
        userListView.setItems(userList);

        File publicKeyFolder = new File("keys/public");
        for (File userKey :
                publicKeyFolder.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        if (name.contains(".asc")) return true;
                        return false;
                    }
                })) {
            int dotPosition = userKey.getName().lastIndexOf(".");

            String fileName = userKey.getName().substring(0, dotPosition);
            userList.add(new User(fileName));

        }

//            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
//
//            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
//            Document document = documentBuilder.parse("keys/public/users.xml");
//            Element root = document.getDocumentElement();
//
//            NodeList users = root.getElementsByTagName("User");
//            for (int i = 0; i < users.getLength(); i++) {
//                Element user = (Element) users.item(i);
//                userList.add(new User(user.getElementsByTagName("UserName").item(0).getTextContent()));
//            }


        userListView.setCellFactory(CheckBoxListCell.forListView(new Callback<User, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(User item) {
                BooleanProperty observable = new SimpleBooleanProperty();
                observable.addListener((obs, wasSelected, isNowSelected) ->
                        {
                            if (isNowSelected) recipients.add(item);
                            else recipients.remove(item);
                        }
                );
                return observable;
            }


        }));

    }

    /**
     * limits key length value both from slider and text field.
     * Value limit is <128,448>
     *
     * @param newLength new length value
     */
    private void keyLengthChanged(int newLength) {
        if (newLength < 128) newLength = 128;
        if (newLength > 448) newLength = 448;
        newLength += (8 - (newLength % 8)) % 8; //round to get multiply by 8
        keyLengthTextField.setText(String.valueOf(newLength));
        keyLengthSlider.setValue(newLength);
    }

    /**
     * Limits subBlock length both on slider and text field when changed manually. Also round value to be multiple by 8
     * Value limit is <8,64)
     *
     * @param newLength
     */
    private void subBlockLengthChanged(int newLength) {

        if (newLength < 8) newLength = 8;
        if (newLength >= 64) newLength = 56;
        newLength = (int) (Math.round(newLength / 8.0) * 8);
        subBlockLengthSlider.setValue(newLength);
        subBlockLengthTextField.setText(String.valueOf(newLength));
    }

    private void encryptionModeChanged(EncryptionMode mode) {
        switch (mode) {
            case ECB:
                subBlockGridPane.setVisible(false);
                break;
            case CBC:
                subBlockGridPane.setVisible(false);
                break;
            case OFB:
                subBlockGridPane.setVisible(true);
                break;
            case CFB:
                subBlockGridPane.setVisible(true);
                break;
            default:
        }
        encryptionModeChoiceBox.setValue(mode);

    }

    private void modeChanged(String newValue) {

        initFiles();
        clearUserList();
        privateKeyPasswordField.clear();
        if (newValue.contains("Encrypt")) {
            clearRecipients();
            initUserList();
            activeMode = Mode.encrypt;
        }
        if (newValue.contains("Decrypt")) {
            activeMode = Mode.decrypt;
        }
    }

    private void clearRecipients() {
        recipients.clear();
    }

    /**
     * Handle manual length (key/subBlock) changing.
     *
     * @param event
     */
    @FXML
    @SuppressWarnings("unused")
    private void handleLengthChanged(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            if (event.getSource() == keyLengthTextField)
                keyLengthChanged(Integer.valueOf(keyLengthTextField.getText()));
            else if (event.getSource() == subBlockLengthTextField)
                subBlockLengthChanged(Integer.valueOf(subBlockLengthTextField.getText()));
        }
    }

    /**
     * Open file chooser to pick file. Sets selected file as input file.
     * Sets input file text field text value at path fo file.
     * <p>
     * If decryptData mode is active reads data from file to find recipients of file.
     */
    @FXML
    @SuppressWarnings("unused")
    private void handlePickInputFile() {
        FileChooser fileChooser = new FileChooser();
        File chosenFile = fileChooser.showOpenDialog(stage);
        if (chosenFile == null)
            return;

        inputFile = new EncryptionFile(chosenFile.getPath());
        inputFilePath.setText(inputFile.getAbsolutePath());
        if (activeMode == Mode.decrypt) {
            clearRecipients();
            clearUserList();
            userListView.getItems().addAll(getRecipientsFromHeader());
        }


    }

    /**
     * Open file chooser to pick file. Sets selected file as a output file.
     * Sets output file textField text value at path fo file.
     */
    @FXML
    @SuppressWarnings("unused")
    private void handlePickOutputFile() {
        FileChooser fileChooser = new FileChooser();
        File chosenFile = fileChooser.showSaveDialog(stage);
        if (chosenFile == null) return;
        outputFile = new EncryptionFile(chosenFile.getPath());
        outputFilePath.setText(outputFile.getAbsolutePath());
    }

    @FXML
    @SuppressWarnings("unused")
    private void handleEncrypt() {
        new Thread(this::encryptFile).start();
    }

    private void encryptFile() {
        if (!areFilesChosen()) return;
        outputFile.delete();

        try {
            EncryptionMode activeEncryptionMode = encryptionModeChoiceBox.getSelectionModel().getSelectedItem();

            SecretKey key = generateKey();

            IvParameterSpec IV = generateIV(activeEncryptionMode);

            int subBlockLength = getSubBlockLength(activeEncryptionMode);

            EncryptionData encryptionData = new EncryptionData(activeEncryptionMode, IV, key, subBlockLength);

            encryptionData.setKeyLength(((int) keyLengthSlider.getValue()));

            encryptData(encryptionData);

            finishEncryption();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private SecretKey generateKey() throws NoSuchAlgorithmException {
//        status.setText("Generating key");
//        log.print("Generating Key ");
//        log.startLoading();

        SecretKey key = Blowfish.generateKey((int) keyLengthSlider.getValue());

//        log.stopLoading();
//        log.println("Done");
        return key;
    }

    private IvParameterSpec generateIV(EncryptionMode activeEncryptionMode) {
        IvParameterSpec IV = null;
        if (activeEncryptionMode != EncryptionMode.ECB) {
            status.setText("Generating IV");

            log.print("Generating IV");
            log.startLoading();

            IV = Blowfish.generateIV();

            log.stopLoading();
            log.println("Done");
        }
        return IV;
    }

    private int getSubBlockLength(EncryptionMode activeEncryptionMode) {
        int subBlockLength = 0;
        if (activeEncryptionMode == EncryptionMode.CFB || activeEncryptionMode == EncryptionMode.OFB) {
            subBlockLength = ((int) subBlockLengthSlider.getValue());
        }
        return subBlockLength;
    }

    private void encryptData(EncryptionData encryptionData) {
        status.setText("Encrypting data");
        log.print("Encrypting data ");
        log.startLoading();

        FileStreamEncrypter encrypter = new FileStreamEncrypter(inputFile, outputFile);

        encrypter.encrypt(encryptionData, recipients);

        log.stopLoading();
        log.println("Done");

    }

    private void finishEncryption() {
        finishProcess("Encryption Completed");
    }

    @FXML
    @SuppressWarnings("unused")
    private void handleDecrypt() {
        new Thread(this::decryptFile).start();
    }

    private void decryptFile() {
        if (!areFilesChosen()) return;

        outputFile.delete();

        EncryptionData encryptionData = getEncryptionDataFromHeader(userListView.getSelectionModel().getSelectedItem(), privateKeyPasswordField.getText());

        decryptData(encryptionData);

        finishDecryption();


    }

    private boolean areFilesChosen() {
        if (!isFileChosen(inputFile)) {
            log.println("No input file");
            return false;
        }
        if (!isFileChosen(outputFile)) {
            log.println("No output file");
            return false;
        }
        return true;
    }

    private void decryptData(EncryptionData encryptionData) {
        status.setText("Decrypting");
        log.print("Decrypting ");
        log.startLoading();

        FileStreamEncrypter encrypter = new FileStreamEncrypter(inputFile, outputFile);
        encrypter.skipHeaderAndDecrypt(encryptionData);

        log.stopLoading();
        log.println("Done");
    }

    private void finishDecryption() {
        finishProcess("Decryption Complete");
    }

    private void finishProcess(String s) {
        log.println(s);
        status.setText(s);
    }

    private boolean isFileChosen(EncryptionFile file) {
        return file != null;

    }

    private void clearUserList() {
        userList.clear();
        userListView.setCellFactory(null);

    }

    /**
     *
     */
    private ArrayList<User> getRecipientsFromHeader() {

        try {
            EncryptionHeader header = new EncryptionHeader(inputFile.readHeader());
            return header.parseRecipients();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    private EncryptionData getEncryptionDataFromHeader(User recipient, String password) {

        try {
            EncryptionHeader header = new EncryptionHeader(inputFile.readHeader());
            return header.parseEncryptionData(recipient, password);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;

    }


}
