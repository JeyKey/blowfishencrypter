package blowfish.controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/mainWindow.fxml"));

        Parent root = loader.load();
        primaryStage.setTitle("Blowfish");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);


        MainController controller = loader.getController();
        controller.setStage(primaryStage);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
