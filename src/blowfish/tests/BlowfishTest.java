package blowfish.tests;

import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import blowfish.model.encryption.Blowfish;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

/**
 * Created by Jaro on 12/05/16.
 */
public class BlowfishTest {
    @Test
    public void testEncryption() throws Exception {
        byte[] data = new byte[100];
        for (int i = 0; i < data.length; i++) {
            data[i] = ((byte) i);
        }


        ByteInputStream input = new ByteInputStream(data, data.length);
        EncryptionData encryptionData = new EncryptionData(EncryptionMode.CFB, Blowfish.generateIV(), Blowfish.generateKey(128), 24);
        ByteOutputStream outputStream = new ByteOutputStream(200);

        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, Blowfish.getInstanceForEncryption(encryptionData));

        byte[] buff = new byte[48];
        while (input.read(buff) > -1) {
            cipherOutputStream.write(buff);
        }
        byte[] encrypted = outputStream.getBytes();

        outputStream = new ByteOutputStream(200);
        input = new ByteInputStream(encrypted, encrypted.length);
        CipherInputStream cipherInputStream = new CipherInputStream(input, Blowfish.getInstanceForDecryption(encryptionData));

        while (cipherInputStream.read(buff) > -1) {
            outputStream.write(buff);
        }

        byte[] decrypted = outputStream.getBytes();


        Assert.assertArrayEquals(data, decrypted);
    }
}
