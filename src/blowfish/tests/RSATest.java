package blowfish.tests;

import blowfish.model.encryption.Blowfish;
import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by Jaro on 20/04/16.
 */
public class RSATest {
    private void skipJCEPolicy() {

        try {
            ignoreJCERestriction();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void ignoreJCERestriction() throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
        field.setAccessible(true);
        field.set(null, Boolean.FALSE);
    }

    @Test
    public void testKeyGen() throws Exception {

        skipJCEPolicy();

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        PrivateKey privateKey = keyPair.getPrivate();

        byte[] data = privateKey.getEncoded();
        byte[] passwdData = MessageDigest.getInstance("SHA-1").digest("haslo".getBytes());

        byte[] encryptedKey = Blowfish.encrypt(data, new EncryptionData(EncryptionMode.ECB, null, Blowfish.getKeyFromBytes(passwdData)));

        Files.write(Paths.get(new File("test").getAbsolutePath()), encryptedKey);
        byte[] keyFromFile = Files.readAllBytes(Paths.get(new File("test").getAbsolutePath()));

        byte[] decryptedKey = Blowfish.decrypt(keyFromFile, new EncryptionData(EncryptionMode.ECB, null, Blowfish.getKeyFromBytes(passwdData)));


        KeyFactory kf = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(decryptedKey);
        PrivateKey newPrivateKey = kf.generatePrivate(ks);


        System.out.println(privateKey);
        System.out.println(newPrivateKey);
    }

    @Test
    public void test() throws Exception {
        SecretKey key = Blowfish.getKeyFromString("asdfgasdfgasdgdsgsdgsgsdgsa");
        System.out.println(key);
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();


        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

        byte[] encryptedBytes = cipher.doFinal(key.getEncoded());
        SecretKey encrypted = Blowfish.getKeyFromBytes(encryptedBytes);


        Cipher cipher2 = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher2.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());


        byte[] decryptedBytes = cipher2.doFinal(encryptedBytes);
        decryptedBytes = Arrays.copyOfRange(decryptedBytes, decryptedBytes.length - 20, decryptedBytes.length);
        SecretKey decrypted = Blowfish.getKeyFromBytes(decryptedBytes);

        System.out.println(decrypted);

        Assert.assertArrayEquals(key.getEncoded(), decrypted.getEncoded());
    }

    @Test
    public void testPGP() throws Exception {
        SecretKey key = Blowfish.getKeyFromString("asdfgasdfgasdgdsgsdgsgsdgsa");
        System.out.println(key);


    }


}
