package blowfish.tests.encryptionMode;

import blowfish.model.EncryptionHeader;
import blowfish.model.EncryptionMode;
import blowfish.model.FileStreamEncrypter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;

/**
 * Created by Jaro on 15/05/16.
 */
public class HeaderTest extends EncryptionModeTest {

    public static String extension = "jpg";

    public HeaderTest(EncryptionMode encryptionMode, int keyLength, int subBlockLength) throws Exception {
        super(encryptionMode, keyLength, subBlockLength);

    }

    @Before
    public void init() throws Exception {
        super.skipJCEPolicy();
        super.initUsers();
        initFiles(getClass().getClassLoader().getResource("resources/test/"), extension);
        super.deleteOutputFiles();

    }

    @After
    public void cleanFiles() throws Exception {
        super.deleteOutputFiles();
    }

    @Test
    public void test() throws Exception {
        encrypt();
        decrypt();
        super.testEncryptedData();
    }

    public void encrypt() throws NoSuchAlgorithmException {
        encrypter = new FileStreamEncrypter(beforeEncryption, buffer);
        encrypter.encrypt(encryptionData, recipients);
    }


    public void decrypt() throws Exception {
        EncryptionHeader header = new EncryptionHeader(buffer.readHeader());

        encrypter = new FileStreamEncrypter(buffer, afterEncryption);
        encrypter.skipHeaderAndDecrypt(header.parseEncryptionData(recipients.get(0), "haslo"));
    }
}
