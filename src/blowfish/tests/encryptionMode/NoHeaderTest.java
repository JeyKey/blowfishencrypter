package blowfish.tests.encryptionMode;

import blowfish.model.EncryptionMode;
import blowfish.model.FileStreamEncrypter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;

/**
 * Created by Jaro on 15/05/16.
 */
public class NoHeaderTest extends EncryptionModeTest {


    public static String extension = "jpg";

    public NoHeaderTest(EncryptionMode encryptionMode, int keyLength, int subBlockLength) throws Exception {
        super(encryptionMode, keyLength, subBlockLength);
    }

    @Before
    public void init() throws Exception {
        super.skipJCEPolicy();
        super.initFiles(getClass().getClassLoader().getResource("resources/test/"), extension);
        super.initUsers();
        super.deleteOutputFiles();
    }

    @After
    public void cleanFiles() throws Exception {
        super.deleteOutputFiles();
    }

    @Test
    public void test() throws Exception {

        encrypt();
        decrypt();
        super.testEncryptedData();
    }

    public void encrypt() throws NoSuchAlgorithmException {
        encrypter = new FileStreamEncrypter(beforeEncryption, buffer);
        encrypter.encrypt(encryptionData);
    }


    public void decrypt() {
        encrypter = new FileStreamEncrypter(buffer, afterEncryption);
        encrypter.decrypt(encryptionData);
    }
}
