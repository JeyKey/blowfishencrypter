package blowfish.tests.encryptionMode.noHeader;

import blowfish.model.EncryptionMode;
import blowfish.tests.encryptionMode.NoHeaderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Jaro on 15/05/16.
 */
@RunWith(Parameterized.class)
public class OFB extends NoHeaderTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {EncryptionMode.OFB, 128, 0},
                {EncryptionMode.OFB, 448, 8},
                {EncryptionMode.OFB, 256, 16},
                {EncryptionMode.OFB, 312, 24},
                {EncryptionMode.OFB, 256, 32},
                {EncryptionMode.OFB, 256, 40},
                {EncryptionMode.OFB, 256, 48},
                {EncryptionMode.OFB, 256, 56},
        });
    }

    public OFB(EncryptionMode encryptionMode, int keyLength, int subBlockLength) throws Exception {
        super(encryptionMode, keyLength, subBlockLength);
    }
}
