package blowfish.tests.encryptionMode.noHeader;

import blowfish.model.EncryptionMode;
import blowfish.tests.encryptionMode.NoHeaderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Jaro on 15/05/16.
 */
@RunWith(Parameterized.class)
public class CFB extends NoHeaderTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {EncryptionMode.CFB, 128, 0},
                {EncryptionMode.CFB, 448, 8},
                {EncryptionMode.CFB, 256, 16},
                {EncryptionMode.CFB, 312, 24},
                {EncryptionMode.CFB, 256, 32},
                {EncryptionMode.CFB, 256, 40},
                {EncryptionMode.CFB, 256, 48},
                {EncryptionMode.CFB, 256, 56},
        });
    }

    public CFB(EncryptionMode encryptionMode, int keyLength, int subBlockLength) throws Exception {
        super(encryptionMode, keyLength, subBlockLength);
    }
}
