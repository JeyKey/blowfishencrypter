package blowfish.tests.encryptionMode.noHeader;

import blowfish.model.EncryptionMode;
import blowfish.tests.encryptionMode.NoHeaderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Jaro on 15/05/16.
 */
@RunWith(Parameterized.class)
public class CBC extends NoHeaderTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {EncryptionMode.CBC, 128},
                {EncryptionMode.CBC, 448},
                {EncryptionMode.CBC, 256},
                {EncryptionMode.CBC, 312},
                {EncryptionMode.CBC, 192}
        });
    }

    public CBC(EncryptionMode encryptionMode, int keyLength) throws Exception {
        super(encryptionMode, keyLength, 0);
    }
}
