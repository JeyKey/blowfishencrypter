package blowfish.tests.encryptionMode.withHeader;

import blowfish.model.EncryptionMode;
import blowfish.tests.encryptionMode.HeaderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Jaro on 15/05/16.
 */
@RunWith(Parameterized.class)
public class ECB extends HeaderTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {EncryptionMode.ECB, 128},
                {EncryptionMode.ECB, 448},
                {EncryptionMode.ECB, 256},
                {EncryptionMode.ECB, 312},
                {EncryptionMode.ECB, 192}
        });
    }

    public ECB(EncryptionMode encryptionMode, int keyLength) throws Exception {
        super(encryptionMode, keyLength, 0);
    }
}
