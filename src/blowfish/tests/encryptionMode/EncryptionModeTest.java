package blowfish.tests.encryptionMode;

import blowfish.model.*;
import blowfish.model.encryption.Blowfish;
import org.junit.Assert;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

abstract public class EncryptionModeTest {

    protected EncryptionFile beforeEncryption;
    protected EncryptionFile afterEncryption;
    protected EncryptionFile buffer;
    protected FileStreamEncrypter encrypter;
    protected EncryptionData encryptionData;
    protected ArrayList<User> recipients;


    public EncryptionModeTest(EncryptionMode encryptionMode, int keyLength, int subBlockLength) throws Exception {
        switch (encryptionMode) {
            case ECB: {
                encryptionData = new EncryptionData(encryptionMode, Blowfish.generateKey(keyLength));
                break;
            }
            case CBC: {
                encryptionData = new EncryptionData(encryptionMode, Blowfish.generateIV(), Blowfish.generateKey(keyLength));
                break;
            }
            case CFB:
            case OFB: {
                encryptionData = new EncryptionData(encryptionMode, Blowfish.generateIV(), Blowfish.generateKey(keyLength), subBlockLength);
                break;
            }
        }
        encryptionData.setKeyLength(keyLength);
    }

    protected void initFiles(URL parentFolder, String extension) throws URISyntaxException, IOException {

        beforeEncryption = new EncryptionFile(new File(parentFolder.toURI()), "test." + extension);
        afterEncryption = new EncryptionFile(new File(parentFolder.toURI()), "test2." + extension);
        buffer = new EncryptionFile(new File(parentFolder.toURI()), "test");

        deleteOutputFiles();

    }

    protected void initUsers() {

        recipients = new ArrayList<>();
        recipients.add(new User("test"));
    }

    protected void deleteOutputFiles() {
        afterEncryption.delete();
        afterEncryption.delete();
        buffer.delete();
    }

    protected void testEncryptedData() throws Exception {
        Assert.assertEquals(beforeEncryption.length(), afterEncryption.length());
        int BUFF = 2 << 5;
        FileInputStream beforefis = new FileInputStream(beforeEncryption);
        FileInputStream afterfis = new FileInputStream(afterEncryption);
        byte[] data = new byte[BUFF];
        int length;
        while (((length = beforefis.read(data)) != -1)) {
            byte[] data2 = new byte[BUFF];
            afterfis.read(data2);
            Assert.assertArrayEquals(Arrays.copyOfRange(data, 0, length), Arrays.copyOfRange(data2, 0, length));
        }
    }

    protected void skipJCEPolicy() throws Exception {

        Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
        field.setAccessible(true);
        field.set(null, Boolean.FALSE);
    }

    abstract public void encrypt() throws Exception;

    abstract public void decrypt() throws Exception;
}