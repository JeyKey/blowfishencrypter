package blowfish.tests;

import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import blowfish.model.encryption.Blowfish;
import blowfish.model.encryption.SHA1;
import blowfish.model.encryption.rsa.RSAKeySaver;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.*;
import java.util.Arrays;

/**
 * Created by Jaro on 11/05/16.
 */
public class RsaPublicKeySaverTest {

    @Test
    public void testSavingToFile() throws Exception {
        PublicKey publicKey = generatePublicKey();

        RSAKeySaver.saveToFile(publicKey, "test.asc");

        PublicKey readPublicKey = RSAKeySaver.loadFromFile("test");
        Assert.assertArrayEquals(publicKey.getEncoded(), readPublicKey.getEncoded());
    }

    @Test
    public void testSavingToXML() throws Exception {
        PublicKey publicKey = generatePublicKey();
        RSAKeySaver.saveToXML(publicKey, "users.xml", "test");

        PublicKey readPublicKey = RSAKeySaver.loadFromXML("users.xml", "test");
        Assert.assertArrayEquals(readPublicKey.getEncoded(), publicKey.getEncoded());
    }


    private PublicKey generatePublicKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        return keyPair.getPublic();
    }


}
