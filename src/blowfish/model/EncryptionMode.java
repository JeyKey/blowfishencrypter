package blowfish.model;

/**
 * Created by Jaro on 11/04/16.
 */
public enum EncryptionMode {
        ECB,
        CBC,
        CFB,
    OFB,

}
