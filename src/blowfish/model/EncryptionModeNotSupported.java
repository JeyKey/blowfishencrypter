package blowfish.model;

/**
 * Created by Jaro on 02/04/16.
 */
public class EncryptionModeNotSupported extends Exception {

    public EncryptionModeNotSupported(String s) {
        super(s);
    }
}
