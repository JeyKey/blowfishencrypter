package blowfish.model;


public class User {

    private  String email;


    public User(String email)
    {
        this.email = email;
    }


    @Override
    public String toString() {
        return email;
    }

    public String getEmail() {
        return email;
    }


}
