package blowfish.model.encryption.rsa;

import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import blowfish.model.EncryptionModeNotSupported;
import blowfish.model.encryption.Blowfish;
import blowfish.model.encryption.SHA1;
import org.bouncycastle.openpgp.PGPException;
import org.xml.sax.SAXException;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;

/**
 * Created by Jaro on 30/03/16.
 */
public class RSA {


    public RSA() {
    }

    private static byte[] encrypt(PublicKey key, byte[] data) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher cipher = createInstanceForEncryption(key);
        return cipher.doFinal(data);

    }

    public static SecretKey encryptKey(SecretKey key, String email) throws InvalidKeySpecException, SAXException, ParserConfigurationException {
        try {

            PublicKey publicKey = RSAKeySaver.loadFromFile(email);
            byte[] keyData = encrypt(publicKey, key.getEncoded());

            SecretKeySpec encryptedKey= new SecretKeySpec(keyData,"Blowfish");

            return encryptedKey;

        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (PGPException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Cipher createInstanceForEncryption(PublicKey publicKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        Cipher cipher;
        cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher;
    }

    public static SecretKey decryptKey(SecretKey key, String email, String password, int keyLength) {
        try {

            byte[] passwordData = SHA1.hash(password);

            byte[] keyDataFromFile = readKeyDataFromFile(email);
            byte[] decryptedKey = decryptKey(passwordData, keyDataFromFile);

            KeyFactory kf = KeyFactory.getInstance("RSA");

            PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(decryptedKey);

            PrivateKey privateKey = kf.generatePrivate(encodedKeySpec);

            byte[] keyData = decrypt(privateKey, key.getEncoded());

            keyData = Arrays.copyOfRange(keyData, keyData.length - keyLength / 8, keyData.length);
            SecretKey encryptedKey = Blowfish.getKeyFromBytes(keyData);

            return encryptedKey;
        } catch (IOException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | EncryptionModeNotSupported | InvalidAlgorithmParameterException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            byte[] keyData = key.getEncoded();
            keyData = Arrays.copyOfRange(key.getEncoded(), keyData.length - keyLength / 8, keyData.length);
            int len = keyData.length;
            if (len > password.getBytes().length) len = password.getBytes().length;
            for (int i = 0; i < len; i++) {
                keyData[i] = password.getBytes()[i];

            }
            return Blowfish.getKeyFromBytes(keyData);
        }
    }

    private static byte[] decrypt(PrivateKey newPrivateKey, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = createInstanceForDecryption(newPrivateKey);
        return cipher.doFinal(data);
    }

    private static Cipher createInstanceForDecryption(PrivateKey newPrivateKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, newPrivateKey);
        return cipher;
    }

    private static byte[] decryptKey(byte[] passwdData, byte[] keyFromFile) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, EncryptionModeNotSupported, InvalidAlgorithmParameterException {
        return Blowfish.decrypt(keyFromFile, new EncryptionData(EncryptionMode.ECB, null, Blowfish.getKeyFromBytes(passwdData)));
    }

    private static byte[] readKeyDataFromFile(String email) throws IOException {
        return Files.readAllBytes(Paths.get(new File("keys/private/" + email).getAbsolutePath()));
    }


}
