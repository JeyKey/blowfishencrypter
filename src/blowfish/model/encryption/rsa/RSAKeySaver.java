package blowfish.model.encryption.rsa;

import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import blowfish.model.encryption.Blowfish;
import blowfish.model.encryption.SHA1;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.jcajce.JcaPGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.jcajce.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by Jaro on 11/05/16.
 */
public class RSAKeySaver {


    public static void saveToXML(PublicKey key, String fileName, String userName) throws NoSuchAlgorithmException, InvalidKeySpecException, ParserConfigurationException, IOException, SAXException, TransformerException {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec publicKeySpec = factory.getKeySpec(key, RSAPublicKeySpec.class);
        BigInteger modulus = publicKeySpec.getModulus();
        BigInteger exponent = publicKeySpec.getPublicExponent();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(fileName);

        Element root = document.getDocumentElement();

        Element user = document.createElement("User");

        Element userNameElement = document.createElement("UserName");
        userNameElement.appendChild(document.createTextNode(userName));
        user.appendChild(userNameElement);

        Element modulusElement = document.createElement("Modulus");
        modulusElement.appendChild(document.createTextNode(Base64.getEncoder().encodeToString(modulus.toByteArray())));
        user.appendChild(modulusElement);


        Element exponentElement = document.createElement("Exponent");
        exponentElement.appendChild(document.createTextNode(Base64.getEncoder().encodeToString(exponent.toByteArray())));
        user.appendChild(exponentElement);

        root.appendChild(user);

        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(fileName);
        transformer.transform(source, result);

    }

    public static void saveToFile(PublicKey key, String fileName) throws IOException {

        FileOutputStream fos = new FileOutputStream(new File(fileName));
        fos.write(key.getEncoded());
        fos.close();
    }

    public static PublicKey loadFromFile(String fileName) throws IOException, ClassNotFoundException, PGPException, NoSuchAlgorithmException {

        PGPPublicKey pgpPublicKey = readPGPPublicKey(new FileInputStream("keys/public/" + fileName + ".asc"));
        JcaPGPKeyConverter converter = new JcaPGPKeyConverter();
        return converter.getPublicKey(pgpPublicKey);


    }

    public static PublicKey loadFromXML(String fileName, String userName) throws IOException, SAXException, ParserConfigurationException, InvalidKeySpecException, NoSuchAlgorithmException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(fileName);

        NodeList users = document.getElementsByTagName("User");
        for (int i = 0; i < users.getLength(); i++) {
            Element user = (Element) users.item(i);
            if (userName.equals(user.getElementsByTagName("UserName").item(0).getTextContent())) {
                NodeList modulusElement = user.getElementsByTagName("Modulus");
                BigInteger modulus = new BigInteger(Base64.getDecoder().decode(modulusElement.item(0).getTextContent()));

                NodeList exponentElement = user.getElementsByTagName("Exponent");

                BigInteger exponent = new BigInteger(Base64.getDecoder().decode(exponentElement.item(0).getTextContent()));

                RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(modulus, exponent);
                KeyFactory factory = KeyFactory.getInstance("RSA");
                return factory.generatePublic(publicKeySpec);
            }
        }
        return null;


    }

    private static PGPPublicKey readPGPPublicKey(InputStream in) throws IOException, PGPException, NoSuchAlgorithmException {


        in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

        JcaPGPPublicKeyRingCollection pgpPub = new JcaPGPPublicKeyRingCollection(in);
        in.close();

        PGPPublicKey key = null;
        Iterator<PGPPublicKeyRing> rIt = pgpPub.getKeyRings();
        while (key == null && rIt.hasNext()) {
            PGPPublicKeyRing kRing = rIt.next();
            Iterator<PGPPublicKey> kIt = kRing.getPublicKeys();
            while (key == null && kIt.hasNext()) {
                PGPPublicKey k = kIt.next();

                if (k.isEncryptionKey()) {
                    key = k;
                }
            }
        }
        return key;


    }

    private static PGPPrivateKey readPGPPRivateKey(InputStream in) throws IOException, PGPException, NoSuchAlgorithmException {


        Security.addProvider(new BouncyCastleProvider());
        in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

        JcaPGPSecretKeyRingCollection pgpPub = new JcaPGPSecretKeyRingCollection(in);
        in.close();

        PGPSecretKey key = null;
        Iterator<PGPSecretKeyRing> rIt = pgpPub.getKeyRings();
        while (key == null && rIt.hasNext()) {
            PGPSecretKeyRing kRing = rIt.next();
            Iterator<PGPSecretKey> kIt = kRing.getSecretKeys();
            while (key == null && kIt.hasNext()) {
                PGPSecretKey k = kIt.next();

                if (k.isMasterKey()) {
                    key = k;
                }
            }
        }
        return key.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder().setProvider("BC").build("haslo".toCharArray()));


    }

    public void transformPGPPrivateKeyToBin(String file, String user, String password) throws Exception {
        PGPPrivateKey privateKey = readPGPPRivateKey(new FileInputStream(new File(file)));
        JcaPGPKeyConverter converter = new JcaPGPKeyConverter();
        PrivateKey key = converter.getPrivateKey(privateKey);
        savePrivateKey(key, user, password);
    }

    public static void savePrivateKey(PrivateKey privateKey, String user, String password) throws Exception {
        ignoreJCERestriction();
        byte[] data = privateKey.getEncoded();
        data = Arrays.copyOf(data, data.length + (8 - data.length % 8) % 8);
        byte[] passwordData = SHA1.hash(password);
        byte[] encryptedKey = Blowfish.encrypt(data, new EncryptionData(EncryptionMode.ECB, null, Blowfish.getKeyFromBytes(passwordData)));

        Files.write(Paths.get(new File("keys/private/" + user).getAbsolutePath()), encryptedKey, StandardOpenOption.CREATE);


    }


    private static void ignoreJCERestriction() throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
        field.setAccessible(true);
        field.set(null, Boolean.FALSE);
    }

    public static void saveToPGP(KeyPair keyPair, String user) throws IOException, PGPException {


        PGPDigestCalculator pgpDigestCalculator = (new JcaPGPDigestCalculatorProviderBuilder()).build().get(2);
        JcaPGPKeyPair pgpKeyPair = new JcaPGPKeyPair(1, keyPair, new Date());
        PGPSecretKey secretKey = new PGPSecretKey(16, pgpKeyPair, user, pgpDigestCalculator, (PGPSignatureSubpacketVector) null, (PGPSignatureSubpacketVector) null, new JcaPGPContentSignerBuilder(pgpKeyPair.getPublicKey().getAlgorithm(), 2), (new JcePBESecretKeyEncryptorBuilder(3, pgpDigestCalculator)).setProvider("BC").build(user.toCharArray()));

        FileOutputStream fos = new FileOutputStream(new File("keys/public/" + user + ".asc"));
        ArmoredOutputStream aos = new ArmoredOutputStream(fos);

        PGPPublicKey publicKey = secretKey.getPublicKey();
        publicKey.encode(aos);
        aos.close();
    }


}
