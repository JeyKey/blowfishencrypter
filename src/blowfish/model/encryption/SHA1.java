package blowfish.model.encryption;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Jaro on 11/05/16.
 */
public class SHA1 {

    public static byte[] hash(String message) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("SHA-1").digest(message.getBytes());
    }
}
