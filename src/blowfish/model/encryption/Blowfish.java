package blowfish.model.encryption;

import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import blowfish.model.EncryptionModeNotSupported;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class Blowfish {

    private static final int BLOCK_SIZE=8;

    /**
     * Encrypt data from file.
     *
     * @param file Existing file to be encrypted
     * @return byte array of encrypted data
     * @throws IOException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     */
    public byte[] encrypt(File file,EncryptionData encryptionData) throws IOException, BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, EncryptionModeNotSupported, InvalidAlgorithmParameterException {
        byte[] data = Files.readAllBytes(Paths.get(file.getPath()));
        return encrypt(data,encryptionData);
    }

    public static byte[] encrypt(byte[] data, EncryptionData encryptionData) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, EncryptionModeNotSupported, InvalidAlgorithmParameterException {
        Cipher cipher = createCipher(encryptionData.getEncryptionMode());
        cipher.init(Cipher.ENCRYPT_MODE, encryptionData.getKey(),encryptionData.getIV());
        return cipher.doFinal(data);
    }

    /**
     * Decrypts byte arra. Array length must by multiply by 8.
     *
     * @param data byte array of encrypted data
     * @return byte array of decrypted data
     * @throws BadPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     */
    public static byte[] decrypt(byte[] data,EncryptionData encryptionData) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, EncryptionModeNotSupported, InvalidAlgorithmParameterException {
        Cipher cipher = createCipher(encryptionData.getEncryptionMode());
        cipher.init(Cipher.DECRYPT_MODE, encryptionData.getKey(),encryptionData.getIV());
        return cipher.doFinal(data);
    }

    private static Cipher createCipher(String encryptionMode, int subBlockLength) throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, EncryptionModeNotSupported {

        if(encryptionMode == "ECB")
            return Cipher.getInstance("Blowfish/ECB/NoPadding");
        else if(encryptionMode == "CBC")
            return Cipher.getInstance("Blowfish/CBC/NoPadding");
        else if (encryptionMode == "OFB") {
            if (subBlockLength > 0) {
                return Cipher.getInstance("Blowfish/OFB" + subBlockLength + "/NoPadding");
            }
            return Cipher.getInstance("Blowfish/OFB/NoPadding");
        } else if (encryptionMode == "CFB") {
            if (subBlockLength > 0) {
                return Cipher.getInstance("Blowfish/CFB" + subBlockLength + "/NoPadding");
            }
            return Cipher.getInstance("Blowfish/CFB/NoPadding");
        }
        else throw new EncryptionModeNotSupported("No such encryption Mode");
    }

    private static Cipher createCipher(EncryptionMode encryptionMode) throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, EncryptionModeNotSupported, NoSuchPaddingException, InvalidKeyException {
        return createCipher(encryptionMode.name(), 0);
    }

    private static Cipher createCipher(EncryptionMode encryptionMode, int subBlockLength) throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, EncryptionModeNotSupported, NoSuchPaddingException, InvalidKeyException {
        return createCipher(encryptionMode.name(), subBlockLength);
    }

    /**
     * Generating SecretKey for Blowfish
     *
     * @param length length of key in bits
     * @return SecretKey at specified length
     */
    public static SecretKey generateKey(int length) throws NoSuchAlgorithmException{

        long time = System.currentTimeMillis();

        KeyGenerator keyGenerator = KeyGenerator.getInstance("Blowfish");
        SecureRandom random = new SecureRandom();
        random.setSeed(time);
        keyGenerator.init(length);

        SecretKey key = keyGenerator.generateKey();

        return key;
    }

    public static IvParameterSpec generateIV() {
        SecureRandom random = new SecureRandom();
        byte[] iv = new byte[BLOCK_SIZE];
        random.setSeed(System.currentTimeMillis());
        random.nextBytes(iv);
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        return  ivSpec;
    }

    public static SecretKey getKeyFromString(String stringKey){
        byte[] keyData = Base64.getDecoder().decode(stringKey);
        return new SecretKeySpec(keyData, 0, keyData.length, "Blowfish");
    }

    public static IvParameterSpec getIVFromString(String IV){
        byte[] IVdata = Base64.getDecoder().decode(IV);
        return new IvParameterSpec(IVdata);
    }

    public static Cipher getInstanceForEncryption(EncryptionData encryptionData) throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, EncryptionModeNotSupported, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        Cipher cipher = createCipher(encryptionData.getEncryptionMode(), encryptionData.getSubBlockLength());

        cipher.init(Cipher.ENCRYPT_MODE,encryptionData.getKey(),encryptionData.getIV());
        return cipher;
    }

    public static Cipher getInstanceForDecryption(EncryptionData encryptionData) throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, EncryptionModeNotSupported, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        Cipher cipher = createCipher(encryptionData.getEncryptionMode(), encryptionData.getSubBlockLength());

        cipher.init(Cipher.DECRYPT_MODE,encryptionData.getKey(),encryptionData.getIV());
        return cipher;
    }

    public static SecretKey getKeyFromBytes(byte[] data) {
        SecretKey key = new SecretKeySpec(data, 0, data.length, "Blowfish");

        return key;
    }

    public static SecretKey getKeyFromBytes(byte[] data, int size) {

        return new SecretKeySpec(data, 0, size, "Blowfish");
    }
}
