package blowfish.model;

import java.io.*;


public class EncryptionFile extends File {

    public EncryptionFile(String pathname) {
        super(pathname);
    }

    public EncryptionFile(File parent, String child) {
        super(parent, child);
    }

    int readDataChunk(byte[] data, long skippedBytes, int length) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(this);
        int dataReadLength = readData(fileInputStream,data,skippedBytes,length);
        fileInputStream.close();
        return dataReadLength;

    }

    private int readData(InputStream inputStream,byte[]data, long skippedBytes, int length) throws IOException {

        inputStream.skip(skippedBytes);
        return inputStream.read(data,0,length);

    }

    public String readHeader(){
        try {

            StringBuilder stringBuilder = new StringBuilder();
            FileInputStream fis;

            fis = new FileInputStream(this);

            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while (!(line = reader.readLine()).contains("/EncryptedFileHeader")) {
                stringBuilder.append(line);
            }
            stringBuilder.append(line);
            reader.close();
            fis.close();
            return stringBuilder.toString();
        }catch (IOException e) { e.printStackTrace();}
        return null;
    }


    int getHeaderLength(){
        try {
            FileInputStream fis = new FileInputStream(this);
            int headerLength = 0;
            byte[] buff = new byte[10];
            while (fis.read(buff) != -1) {
                headerLength += buff.length;
                while (fis.read() != 10) headerLength++;
                headerLength++;
                if (new String(buff).contains("</En")) break;
            }
            fis.close();
            return headerLength;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }


}
