package blowfish.model;

import blowfish.model.encryption.Blowfish;

import javax.crypto.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

public class FileStreamEncrypter {


    private EncryptionFile inputFile;
    private EncryptionFile outputFile;
    private final int BUFF_SIZE = 1 << 8;//1 << 6;//1 << 8;

    public FileStreamEncrypter(EncryptionFile inputFile, EncryptionFile outputFile) {
        this.inputFile = inputFile; //File stream encryptor
        this.outputFile = outputFile;
    }

    public void encrypt(EncryptionData encryptionData) {
        try {
            Cipher cipher = Blowfish.getInstanceForEncryption(encryptionData);
            encryptStream(cipher);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (EncryptionModeNotSupported encryptionModeNotSupported) {
            encryptionModeNotSupported.printStackTrace();
        }
    }

    private void encryptStream(Cipher cipher) throws IOException, BadPaddingException, IllegalBlockSizeException {
        encryptStream(cipher, 0);
    }

    private void encryptStreamWithHeader(Cipher cipher, EncryptionData encryptionData, ArrayList<User> recipients) throws IOException, BadPaddingException, IllegalBlockSizeException {
        byte[] header = EncryptionHeader.createHeader(recipients, encryptionData);
        FileInputStream fis = new FileInputStream(inputFile);
        FileOutputStream fos = new FileOutputStream(outputFile, true);
        fos.write(header);

        byte[] data = new byte[BUFF_SIZE];
        int len;
        while ((len = fis.read(data, 0, BUFF_SIZE)) != -1) {

            data = cipher.doFinal(data);

            byte[] lenData = ByteBuffer.allocate(4).putInt(len).array();
            fos.write(lenData);
            fos.write(data, 0, BUFF_SIZE);
        }

        System.out.println();
        fos.close();
        fis.close();
    }


    private void encryptStream(Cipher cipher, long skippedBytes) throws IOException, BadPaddingException, IllegalBlockSizeException {
        FileOutputStream fos = new FileOutputStream(outputFile, true);
        FileInputStream fis = new FileInputStream(inputFile);

        byte[] data = new byte[BUFF_SIZE];
        int len;
        while ((len = fis.read(data, 0, BUFF_SIZE)) != -1) {//= BUFF_SIZE) {

            byte[] lenData = ByteBuffer.allocate(4).putInt(len).array();
            data = cipher.doFinal(data);
            fos.write(lenData, 0, 4);
            fos.write(data, 0, BUFF_SIZE);
        }
        System.out.println();

        fos.close();
        fis.close();
    }


    private void decryptStream(Cipher cipher, long skippedBytes) throws IOException, BadPaddingException, IllegalBlockSizeException {
        FileOutputStream fos = new FileOutputStream(outputFile, true);

        FileInputStream fis = new FileInputStream(inputFile);
        byte[] data = new byte[BUFF_SIZE];

        int len;
        byte[] lenBytes = new byte[4];

        int i = 0;
        if (skippedBytes > 0) fis.skip(skippedBytes);

        while ((fis.read(lenBytes, 0, 4)) != -1) {

            len = ByteBuffer.wrap(lenBytes).getInt();
            fis.read(data, 0, BUFF_SIZE);

            data = cipher.doFinal(data);
            fos.write(data, 0, len);

    }

        fos.close();
        fis.close();


    }

    public void encrypt(EncryptionData encryptionData, ArrayList<User> recipients) {
        try {
            Cipher cipher = Blowfish.getInstanceForEncryption(encryptionData);
            encryptStreamWithHeader(cipher, encryptionData, recipients);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (EncryptionModeNotSupported encryptionModeNotSupported) {
            encryptionModeNotSupported.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    private void decrypt(EncryptionData encryptionData, long skippedBytes, String password) {
        try {
            Cipher cipher = Blowfish.getInstanceForDecryption(encryptionData);
            decryptStream(cipher, skippedBytes);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (EncryptionModeNotSupported encryptionModeNotSupported) {
            encryptionModeNotSupported.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void decrypt(EncryptionData encryptionData, long skippedBytes) {
        decrypt(encryptionData, skippedBytes, null);

    }

    public void decrypt(EncryptionData encryptionData) {
        decrypt(encryptionData, 0);
    }

    public void skipHeaderAndDecrypt(EncryptionData encryptionData) {
        int headerLength = inputFile.getHeaderLength();
        decrypt(encryptionData, headerLength);
    }

}
