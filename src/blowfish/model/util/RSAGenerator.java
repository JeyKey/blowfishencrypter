package blowfish.model.util;

import blowfish.model.EncryptionData;
import blowfish.model.EncryptionMode;
import blowfish.model.EncryptionModeNotSupported;
import blowfish.model.encryption.*;
import blowfish.model.encryption.rsa.RSAKeySaver;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPException;
import org.xml.sax.SAXException;

import javax.crypto.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Jaro on 19/04/16.
 */
public class RSAGenerator {

    final static String PASSWORD = "haslo";

    public static void main(String[] args) throws Exception {

        org.bouncycastle.openpgp.examples.RSAKeyPairGenerator.main(args);
        ignoreJCERestriction();
        Security.addProvider(new BouncyCastleProvider());
        Scanner in = new Scanner(System.in);
        while ((in.hasNext())) {

            String line = in.nextLine();
            String user = line.split(" ")[0];

            String password = "haslo";
            if (line.split(" ").length > 1) password = line.split(" ")[1];
            generateAndSaveKeys(user, password);


        }


    }


    private static void generateAndSaveKeys(String user, String password) throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();


        savePGPPublicKey(keyPair, user);
        savePrivateKey(keyPair.getPrivate(), user, password);
    }

    private static void savePGPPublicKey(KeyPair keyPair, String user) throws PGPException, IOException {
        RSAKeySaver.saveToPGP(keyPair, user);
    }

    private static void savePrivateKey(PrivateKey privateKey, String user, String password) throws Exception {
        RSAKeySaver.savePrivateKey(privateKey, user, password);


    }

    private static void savePublicKey(PublicKey publicKey, String user) throws IOException, InvalidKeySpecException, ParserConfigurationException, NoSuchAlgorithmException, SAXException, TransformerException {
        RSAKeySaver.saveToXML(publicKey, "keys/public/users.xml", user);

    }



    private static void ignoreJCERestriction() throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
        field.setAccessible(true);
        field.set(null, Boolean.FALSE);
    }

}
