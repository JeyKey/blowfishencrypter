package blowfish.model;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.security.auth.DestroyFailedException;


public class EncryptionData {
    private IvParameterSpec IV;
    private EncryptionMode encryptionMode;
    private SecretKey key;
    private int subBlockLength;
    private int keyLength;

    public EncryptionData(EncryptionMode encryptionMode, SecretKey key) {
        this(encryptionMode, null, key, 0);
    }

    public EncryptionData(EncryptionMode encryptionMode, IvParameterSpec IV, SecretKey key) {
        this(encryptionMode, IV, key, 0);
    }

    public EncryptionData(EncryptionMode encryptionMode, IvParameterSpec IV, SecretKey key, int subBlockLength) {
        this.IV = IV;
        this.encryptionMode = encryptionMode;
        this.key = key;
        this.subBlockLength = subBlockLength;


    }

    public EncryptionMode getEncryptionMode() {
        return encryptionMode;
    }

    public SecretKey getKey() {
        return key;
    }

    public IvParameterSpec getIV() {
        return IV;
    }


    public int getSubBlockLength() {
        return subBlockLength;
    }

    public void destroy() throws DestroyFailedException {
        key.destroy();
        IV = null;
        subBlockLength = -1;
        encryptionMode = null;
    }

    public int getKeyLength() {
        return keyLength;
    }


    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }
}