package blowfish.model;

import blowfish.model.encryption.Blowfish;
import blowfish.model.encryption.rsa.RSA;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;

public class EncryptionHeader {
    private String header;

    public EncryptionHeader(String header) {
        this.header = header;
    }

    public EncryptionData parseEncryptionData(User recipient, String password) throws ParserConfigurationException, IOException, SAXException {
        Document document = getDocumentFromString();

        EncryptionMode encryptionMode = parseCipherMode(document);

        IvParameterSpec IV = parseIV(document, encryptionMode);

        String sessionKey = parseKey(document, recipient, password);

        int keyLength = parseKeyLength(document);
        SecretKey secretKey = RSA.decryptKey(Blowfish.getKeyFromString(sessionKey), recipient.getEmail(), password, keyLength);

        int subBlock = parseSubBlock(document);

        EncryptionData encryptionData = new EncryptionData(encryptionMode, IV, secretKey, subBlock);
        encryptionData.setKeyLength(keyLength);

        return encryptionData;
    }



    private int parseKeyLength(Document document) {

        NodeList keyLengthElement = document.getElementsByTagName("KeyLength");
        int keyLength = Integer.parseInt(keyLengthElement.item(0).getTextContent());

        return keyLength;
    }

    private int parseSubBlock(Document document) {
        int subBlockLength = 0;

        NodeList subBlock = document.getElementsByTagName("SubBlock");
        if (subBlock.getLength() > 0) {
            subBlockLength = Integer.parseInt(subBlock.item(0).getTextContent());
        }
        return subBlockLength;
    }

    public ArrayList<User> parseRecipients() throws IOException, SAXException, ParserConfigurationException {
        ArrayList<User> recipients = new ArrayList<>();

        Document document = getDocumentFromString();

        NodeList users = document.getElementsByTagName("User");
        for (int i = 0; i < users.getLength(); i++) {
            Element user = ((Element) users.item(i));
            String email = user.getElementsByTagName("Email").item(0).getTextContent();

            recipients.add(new User(email));
        }

        return recipients;
    }

    private String parseKey(Document doc, User recipient, String password) {
        NodeList recipientNodes = doc.getElementsByTagName("User");
        for (int i = 0; i < recipientNodes.getLength(); i++) {
            Element userInfo = (Element) recipientNodes.item(i);

            String email = userInfo.getElementsByTagName("Email").item(0).getTextContent();
            if (recipient.getEmail().equals(email)) {
                String sessionKey = userInfo.getElementsByTagName("SessionKey").item(0).getTextContent();

                return sessionKey;

            }

        }
        return null;
    }

    private IvParameterSpec parseIV(Document doc, EncryptionMode activeEncryptionMode) {
        IvParameterSpec IV = null;
        if (activeEncryptionMode != EncryptionMode.ECB) {
            NodeList iv = doc.getElementsByTagName("IV");
            IV = Blowfish.getIVFromString(iv.item(0).getTextContent());
        }
        return IV;
    }

    private EncryptionMode parseCipherMode(Document doc) {
        NodeList mode = doc.getElementsByTagName("CipherMode");
        return EncryptionMode.valueOf(mode.item(0).getTextContent());
    }

    private Document getDocumentFromString() throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(new InputSource(new StringReader(header)));
    }

    public static byte[] createHeader(ArrayList<User> recipients, EncryptionData encryptionData) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.newDocument();

            Element root = doc.createElement("EncryptedFileHeader");
            doc.appendChild(root);

            Element algorithm = doc.createElement("Algorithm");
            algorithm.appendChild(doc.createTextNode("Blowfish"));
            root.appendChild(algorithm);

            Element keySize = doc.createElement("KeyLength");
            keySize.appendChild(doc.createTextNode(String.valueOf(encryptionData.getKeyLength())));
            root.appendChild(keySize);

            Element cipherMode = doc.createElement("CipherMode");
            cipherMode.appendChild(doc.createTextNode(encryptionData.getEncryptionMode().name()));
            root.appendChild(cipherMode);

            if (encryptionData.getIV() != null) {
                Element iv = doc.createElement("IV");
                iv.appendChild(doc.createTextNode(Base64.getEncoder().encodeToString(encryptionData.getIV().getIV())));
                root.appendChild(iv);
            }

            if (encryptionData.getSubBlockLength() > 0) {
                Element subBlock = doc.createElement("SubBlock");
                subBlock.appendChild(doc.createTextNode(String.valueOf(encryptionData.getSubBlockLength())));
                root.appendChild(subBlock);
            }

            Element approvedUsers = doc.createElement("ApprovedUsers");
            root.appendChild(approvedUsers);

            for (User u : recipients) {

                Element user = doc.createElement("User");
                approvedUsers.appendChild(user);

                Element email = doc.createElement("Email");
                email.appendChild(doc.createTextNode(u.getEmail()));
                user.appendChild(email);

                Element sessionKey = doc.createElement("SessionKey");
                SecretKey encryptedKey = RSA.encryptKey(encryptionData.getKey(), u.getEmail());
                sessionKey.appendChild(doc.createTextNode(Base64.getEncoder().encodeToString(encryptedKey.getEncoded())));
                user.appendChild(sessionKey);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(byteArrayOutputStream);

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            transformer.transform(source, result);

            return byteArrayOutputStream.toByteArray();


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

}
